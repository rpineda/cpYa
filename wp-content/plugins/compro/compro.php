<?php
/*
Plugin Name: Compro Backoffice
Description: Compro Backoffice
Author: Compro
Version: 0.1
*/
const COMPRO_API = 'http://localhost:8080';

if ($_GET['download-amount']) {
    compro_get_amount_discount();
}

if ($_GET['download-product']) {
    compro_get_product_discount();
}

function compro_download_template($content = array(), $header = array(), $filename = 'productos.csv', $delimiter = ',') {
    $file = array();
    $file[] = join(',', $header);
    foreach ($content as $line) {
        $file[] = join(',', $line);
    }

    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false);
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=\"$filename\";" );
    header("Content-Transfer-Encoding: binary");

    echo join($file, "\n");
    die;
}

add_action('admin_menu', 'plugin_setup_menu');

function plugin_setup_menu() {
    add_menu_page( 'Compro Backoffice', 'Descuentos', 'manage_options', 'compro-plugin', 'compro_init' );
    add_submenu_page('compro-plugin',    // Parent Menu
        'Ventas',                // Page Title
        'Ventas',                // Menu Option Label
        'manage_options',                // Capability
        'admin.php?page=compro-plugin&download-amount=1');// Option URL relative to /wp-admin/

    add_submenu_page('compro-plugin',    // Parent Menu
        'Productos',                // Page Title
        'Productos',                // Menu Option Label
        'manage_options',                // Capability
        'admin.php?page=compro-plugin&download-product=1');// Option URL relative to /wp-admin/
}

function compro_init(){
    compro_handle_post();
    ?>
    <h1>Actualización de Descuentos</h1>
    <h2>Subir archivo</h2>
    <!-- Form to handle the upload - The enctype value here is very important -->
    <form  method="post" enctype="multipart/form-data">
        <select name="type">
            <option value="1">Descuentos sobre Ventas</option>
            <option value="2">Descuentos sobre Productos</option>
        </select>
        <input type='file' id='discount_file' name='discount_file' />
        <?php submit_button('Subir') ?>
    </form>
    <?php
}

function compro_handle_post(){
    // First check if the file appears on the _FILES array
    if(isset($_FILES['discount_file'])){
        $file = $_FILES['discount_file'];
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $content = file_get_contents($file['tmp_name']);
        if ($ext == 'csv' && $content !== false) {
            $lines = explode("\r", $content);
            if (count($lines) > 1) {
                $error = false;
                for ($i = 1; $i < count($lines); $i++) {
                    $data = explode(",", $lines[$i]);
                    if (is_array($data)) {
                        $response = $_POST['type'] == 1 ? compro_put_amount_discount($data[0], $data[1], $data[2], $data[3], $data[4]) :
                            compro_put_sku_discount($data[1], $data[2], $data[3], $data[4]);
                        if ($response != '') {
                            $error = true;
                            echo $response;
                        }
                    }
                }

                if (!$error) {
                    echo 'Archivo subido con exito';
                }
            } else {
                echo 'Archivo vacio';
            }
        } else {
            echo 'Error al leer el archivo';
        }
    }
}

function compro_put_amount_discount($id, $percentage, $lowerLimit, $upperLimit, $status) {
    $request = array(
        'id' => $id,
        'percentage' => $percentage,
        'lower_limit' => $lowerLimit,
        'upper_limit' => $upperLimit,
        'status' => $status
    );
    $result = '';

    $curl = curl_init(sprintf('%s/amount/discount',COMPRO_API));
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");

    // Send the request & save response to $resp
    $response = curl_exec($curl);

    if (curl_errno($curl)) {
        $result = sprintf('Error en linea %s, %s, %s, %s', $percentage, $lowerLimit, $upperLimit, $status);
    }

    // Close request to clear up some resources
    curl_close($curl);

    return $result;
}

function compro_put_sku_discount($sku, $percentage, $units, $status) {
    $request = array(
        'sku' => $sku,
        'percentage' => $percentage,
        'units' => $units,
        'status' => $status
    );
    $result = '';

    $curl = curl_init(sprintf('%s/product/discount',COMPRO_API));
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");

    // Send the request & save response to $resp
    $response = curl_exec($curl);

    if (curl_errno($curl)) {
        $result = sprintf('Error en linea %s, %s, %s, %s', $sku, $percentage, $units, $status);
    }

    // Close request to clear up some resources
    curl_close($curl);

    return $result;
}

function compro_get_amount_discount() {
    // Set some options - we are passing in a useragent too here
    $request = array(
        'status' => array('A','I'),
        'skip' => 0
    );
    $result = '';

    $curl = curl_init(sprintf('%s/amount/discount/search',COMPRO_API));
    curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode($request));
    curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

    // Send the request & save response to $resp
    $response = curl_exec($curl);

    if ($response) {
        $result = json_decode($response, true);
    }

    // Close request to clear up some resources
    curl_close($curl);

    $content = $result && is_array($result) ? $result['amounts'] : array();

    compro_download_template($content, array('id', 'porcentaje', 'rango inferior', 'rango superior', 'estado'), 'ventas.csv');

    exit;
}

function compro_get_product_discount() {
    // Set some options - we are passing in a useragent too here
    $request = array(
        'status' => array('A','I'),
        'skip' => 0
    );
    $result = '';

    $curl = curl_init(sprintf('%s/product/discount/search',COMPRO_API));
    curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode($request));
    curl_setopt( $curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

    // Send the request & save response to $resp
    $response = curl_exec($curl);

    if ($response) {
        $result = json_decode($response, true);
    }

    // Close request to clear up some resources
    curl_close($curl);

    $content = $result && is_array($result) ? $result['products'] : array();

    compro_download_template($content, array('id', 'sku', 'porcentaje', 'unidades', 'estado'));

    exit;
}

function compro_post_discount($items) {
    // Set some options - we are passing in a useragent too here
    $request = array(
        'products' => $items
    );

    $discount = 0;

    $curl = curl_init(sprintf('%s/discount',COMPRO_API));
    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($request));
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($curl,CURLOPT_POST, true);

    // Send the request & save response to $resp
    $response = curl_exec($curl);

    if ($response) {
        $result = json_decode($response, true);
        if (is_array($result)) {
            $discount = $result['discount'];
        }
    }

    // Close request to clear up some resources
    curl_close($curl);

    return $discount;

}