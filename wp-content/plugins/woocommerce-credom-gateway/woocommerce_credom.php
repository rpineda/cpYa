<?php

class WC_Credom extends WC_Payment_Gateway {

    // Setup our Gateway's id, description and other values
    function __construct() {

        // The global ID for this Payment method
        $this->id = "wc_credom";

        // The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
        $this->method_title = __( "Credom", 'wc-credom' );

        // The description for this Payment Gateway, shown on the actual Payment options page on the backend
        $this->method_description = __( "Credom Payment Gateway Plug-in for WooCommerce", 'wc-credom' );

        // The title to be used for the vertical tabs that can be ordered top to bottom
        $this->title = __( "Credom", 'wc-credom' );

        // If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
        $this->icon = null;

        // Bool. Can be set to true if you want payment fields to show on the checkout
        // if doing a direct integration, which we are doing in this case
        $this->has_fields = true;

        // Supports the default credit card form
        $this->supports = array( 'default_credit_card_form' );

        // This basically defines your settings which are then loaded with init_settings()
        $this->init_form_fields();

        // After init_settings() is called, you can get the settings and load them into variables, e.g:
        // $this->title = $this->get_option( 'title' );
        $this->init_settings();

        // Turn these settings into variables we can use
        foreach ( $this->settings as $setting_key => $value ) {
            $this->$setting_key = $value;
        }

        // Lets check for SSL
        //add_action( 'admin_notices', array( $this,	'do_ssl_check' ) );

        // Save settings
        if ( is_admin() ) {
            // Versions over 2.0
            // Save our administration options. Since we are not going to be doing anything special
            // we have not defined 'process_admin_options' in this class so the method in the parent
            // class will be used instead
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        }
    } // End __construct()

    // Build the administration fields for this specific Gateway
    public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'title'		=> __( 'Enable / Disable', 'wc-credom' ),
                'label'		=> __( 'Enable this payment gateway', 'wc-credom' ),
                'type'		=> 'checkbox',
                'default'	=> 'no',
            ),
            'title' => array(
                'title'		=> __( 'Title', 'wc-credom' ),
                'type'		=> 'text',
                'desc_tip'	=> __( 'Payment title the customer will see during the checkout process.', 'wc-credom' ),
                'default'	=> __( 'Credit card', 'wc-credom' ),
            ),
            'description' => array(
                'title'		=> __( 'Description', 'wc-credom' ),
                'type'		=> 'textarea',
                'desc_tip'	=> __( 'Payment description the customer will see during the checkout process.', 'wc-credom' ),
                'default'	=> __( 'Pay securely using your credit card.', 'wc-credom' ),
                'css'		=> 'max-width:350px;'
            ),
			
			'key_id' => array(
				'title' 	=> __( 'key_id ', 'wc-credom'),
				'type' 		=> 'text',
				'desc_tip'  => __( 'Credomatic key_id', 'wc-credom'),
				'default' 	=> __('7618500', 'wc-credom'),
			),
			
			'key' 	=> array(
				'title' 	=> __('key', 'wc-credom'),
				'type'		=> 'text',
				'desc_tip'  => __('Credomatic key', 'wc-credom'), 
				'default' 	=> __('25qK48r7D848FavJJvZ2RVwv2mAK9Z88', 'wc-credom'),
			),
			
			'environment_url' => array (
				'title' 	=> __('environment_url', 'wc-credom'),
				'type'		=> 'text',
				'desc_tip'	=> __('environment_url', 'wc-credom'),
				'default'	=> __('https://credomatic.compassmerchantsolutions.com/api/transact.php','wc-credom'),
			
			),
			
			
            /*'api_login' => array(
                'title'		=> __( 'Authorize.net API Login', 'wc-credom' ),
                'type'		=> 'text',
                'desc_tip'	=> __( 'This is the API Login provided by Authorize.net when you signed up for an account.', 'wc-credom' ),
            ),
            'trans_key' => array(
                'title'		=> __( 'Authorize.net Transaction Key', 'wc-credom' ),
                'type'		=> 'password',
                'desc_tip'	=> __( 'This is the Transaction Key provided by Authorize.net when you signed up for an account.', 'wc-credom' ),
            ),
            'environment' => array(
                'title'		=> __( 'Authorize.net Test Mode', 'wc-credom' ),
                'label'		=> __( 'Enable Test Mode', 'wc-credom' ),
                'type'		=> 'checkbox',
                'description' => __( 'Place the payment gateway in test mode.', 'wc-credom' ),
                'default'	=> 'no',
            )*/
        );
    }

    // Submit payment and handle response
    public function process_payment( $order_id ) {
        global $woocommerce;

        // Get this Order's information so that we know
        // who to charge and how much
        $customer_order = new WC_Order( $order_id );

		//Gateway de pago
		$orderId = $order_id;
		$amount = $customer_order->order_total;
		$key = $this->key;
		$time = time();
		$redirect = $this->get_return_url( $customer_order );
		$environment_url = $this->environment_url;
 // Aquí se pone un poco divertido
        $chargeData = array(
            // Credenciales desde el backend
            "key_id"            	=> $this->key_id,
            "hash"              	=> md5($orderId."|".$amount."|".$time."|".$key),
            "type"              	=> "sale",
			"time"					=> $time,
			"redirect"				=> "http://www.compro.dev/gracias",
			"orderid"				=> $orderId,
			"amount"				=> $amount,
             
            // Tarjeta de credito
            "ccnumber"  	        => str_replace( array(' ', '-' ), '', $_POST['wc_credom-card-number'] ),
            "cvv"           		=> ( isset( $_POST['wc_credom-card-cvc'] ) ) ? $_POST['wc_credom-card-cvc'] : '',
            "ccexp"            		=> str_replace( array( '/', ' '), '', $_POST['wc_credom-card-expiry'] ),
        );
     

		$string_field="";
		foreach($chargeData as $position=>$value) {
			$string_field .= $position.'='.$value.'&';
		}
		$string_field = substr($string_field,0,-1);
		//COMIENZA EL CURL
		$ch = curl_init($environment_url);
		//el array del POST se vuelve string para el envío
		curl_setopt($ch, CURLOPT_URL, $environment_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$string_field);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION ,1);
		curl_setopt($ch, CURLOPT_HEADER ,1); // DO NOT RETURN HTTP HEADERS
		curl_setopt($ch, CURLOPT_RETURNTRANSFER ,1); // RETURN THE CONTENTS OF THE CALL
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); // Con 15 segundos tambien funciona (15)
		//EXECUTE !
		$result		 = curl_exec($ch);
		$header = curl_getinfo ( $ch );
		curl_close($ch);
		
//Archivo temporal que tiene el Verify by VISA (3DSecure)
		define('__ROOT__', dirname(dirname(__FILE__))); 
		$fichero = __ROOT__."/../../securedir/name-".$orderId.".html"; // Transacción exitosa
		$fichero_e = __ROOT__."/../../securedir/name-".$orderId.".html"; //Transacción fallida
		
		
		$file = file_put_contents($fichero, $result);
		

		if (strlen($result)>0):
            // Redirect to thank you page
			
			//'redirect'	=> add_query_arg('key', $order->order_key, add_query_arg('order', $order_id, get_permalink(woocommerce_get_page_id('thanks'))))
			$params = explode(":", $result);
			$xret = parse_str($params[6]);
			$xret = explode("&", $params[6]);
			extract($xret);
			
			if ($response_code == 100):
				// Transacción exitosa
				$customer_order->add_order_note( __( 'wcCpg1', 'Credomatic payment completed.' ) );
													  
				// Marcar la orden como pagada
				$customer_order->payment_complete();

				// Reducir items de inventario
				//$customer_order->reduce_order_stock();
				
				// Vaciar el carrito
				$woocommerce->cart->empty_cart();
				return array(
					'result'   => 'success',
					'redirect' => $this->get_return_url( $customer_order ),
				);
			elseif ($response_code == 200):
				// Transacción no exitosa
				// Notificar el usuario
				wc_add_notice( $responsetext, 'error' );
				// Referencia
				$customer_order->add_order_note( 'Error: '. $r['response_reason_text'] );
				$file_e = file_put_contents($fichero_e, $result.print_r($header,true));
				
			else:

                //Wordpress hace el redirect al archivo temporal Verify By Visa y ejecuta la validación de lado del cliente.
				## 3dSecure
				$file = file_put_contents($fichero, $result);
			
				return array(
					'result'   => 'success',
					'redirect' => $this->get_return_url( $customer_order ),
				);
			endif;

        else :
            // Transacción no exitosa
            // Notificar al usuario
            wc_add_notice( 'error', 'error' );
            // Referencia
            $customer_order->add_order_note( 'Error: '. $r['response_reason_text'] );
			$file_e = file_put_contents($fichero_e, $result.print_r($header,true));
        endif;
		
		//End Gateway de pago 
		
        // Get the values we need
        $r['response_code']             = 1;

        // Test the code to know if the transaction went through or not.
        // 1 or 4 means the transaction was a success
        if ( ( $r['response_code'] == 1 ) || ( $r['response_code'] == 4 ) ) {
            // Payment has been successful
            $customer_order->add_order_note( __( 'Credom payment completed.', 'wc-credom' ) );

            // Mark order as Paid
            $customer_order->payment_complete();
            update_post_meta( $customer_order->id, 'transaction_id', wc_clean( uniqid() ) );

            // Empty the cart (Very important step)
            $woocommerce->cart->empty_cart();

            // Redirect to thank you page
            return array(
                'result'   => 'success',
                'redirect' => $this->get_return_url( $customer_order ),
            );
        } else {
            // Transaction was not succesful
            // Add notice to the cart
            wc_add_notice( $r['response_reason_text'], 'error' );
            // Add note to the order for your reference
            $customer_order->add_order_note( 'Error: '. $r['response_reason_text'] );
        }

    }

    // Validate fields
    public function validate_fields() {
        return true;
    }

    // Check if we are forcing SSL on checkout pages
    // Custom function not required by the Gateway
    public function do_ssl_check() {
        if( $this->enabled == "yes" ) {
            if( get_option( 'woocommerce_force_ssl_checkout' ) == "no" ) {
                echo "<div class=\"error\"><p>". sprintf( __( "<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>" ), $this->method_title, admin_url( 'admin.php?page=wc-settings&tab=checkout' ) ) ."</p></div>";
            }
        }
    }
}