<?php 
/*
* Plugin Name: WordPress MyShortCode
* Description: Create your WordPress shortcode.
* Version: 1.0
* Author: Archangel Systems
* Author URI: http://compro.dev
*/

// Shortcult Credom 

function credom_result( $atts ) {
    extract( shortcode_atts( array(), $atts, 'multilink' ) );
	
	//Obtener valores del URL
	$responseText = $_GET["responsetext"];
    $responseCode = $_GET["response_code"];
 
	if ($responseCode==100) {
		
		$xret = '<div class="alert alert-success" role="alert">'. 'Transacción exitosa' . '</div>';
		
	}else{
		
		$xret = '<div class="alert alert-danger" role="alert">' . 'Transacción fallida <br>' .  $responseCode . ' ' . $responseText . ' </div>';
		
	}
	return $xret;
}
	add_shortcode( 'Credomatic_result', 'credom_result' );
?>