<?php

class WC_Visanet extends WC_Payment_Gateway {

    // Setup our Gateway's id, description and other values
    function __construct() {

        // The global ID for this Payment method
        $this->id = "wc_visanet";

        // The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
        $this->method_title = __( "Visanet", 'wc-visanet' );

        // The description for this Payment Gateway, shown on the actual Payment options page on the backend
        $this->method_description = __( "Visanet Payment Gateway Plug-in for WooCommerce", 'wc-visanet' );

        // The title to be used for the vertical tabs that can be ordered top to bottom
        $this->title = __( "Visanet", 'wc-visanet' );

        // If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
        $this->icon = null;

        // Bool. Can be set to true if you want payment fields to show on the checkout
        // if doing a direct integration, which we are doing in this case
        $this->has_fields = true;

        // Supports the default credit card form
        $this->supports = array( 'default_credit_card_form' );

        // This basically defines your settings which are then loaded with init_settings()
        $this->init_form_fields();

        // After init_settings() is called, you can get the settings and load them into variables, e.g:
        // $this->title = $this->get_option( 'title' );
        $this->init_settings();

        // Turn these settings into variables we can use
        foreach ( $this->settings as $setting_key => $value ) {
            $this->$setting_key = $value;
        }

        // Lets check for SSL
        //add_action( 'admin_notices', array( $this,	'do_ssl_check' ) );

        // Save settings
        if ( is_admin() ) {
            // Versions over 2.0
            // Save our administration options. Since we are not going to be doing anything special
            // we have not defined 'process_admin_options' in this class so the method in the parent
            // class will be used instead
            add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
        }
    } // End __construct()

    // Build the administration fields for this specific Gateway
    public function init_form_fields() {
        $this->form_fields = array(
            'enabled' => array(
                'title'		=> __( 'Enable / Disable', 'wc-visanet' ),
                'label'		=> __( 'Enable this payment gateway', 'wc-visanet' ),
                'type'		=> 'checkbox',
                'default'	=> 'no',
            ),
            'title' => array(
                'title'		=> __( 'Title', 'wc-visanet' ),
                'type'		=> 'text',
                'desc_tip'	=> __( 'Payment title the customer will see during the checkout process.', 'wc-visanet' ),
                'default'	=> __( 'Credit card', 'wc-visanet' ),
            ),
            'description' => array(
                'title'		=> __( 'Description', 'wc-visanet' ),
                'type'		=> 'textarea',
                'desc_tip'	=> __( 'Payment description the customer will see during the checkout process.', 'wc-visanet' ),
                'default'	=> __( 'Pay securely using your credit card.', 'wc-visanet' ),
                'css'		=> 'max-width:350px;'
            )
            /*'api_login' => array(
                'title'		=> __( 'Authorize.net API Login', 'wc-visanet' ),
                'type'		=> 'text',
                'desc_tip'	=> __( 'This is the API Login provided by Authorize.net when you signed up for an account.', 'wc-visanet' ),
            ),
            'trans_key' => array(
                'title'		=> __( 'Authorize.net Transaction Key', 'wc-visanet' ),
                'type'		=> 'password',
                'desc_tip'	=> __( 'This is the Transaction Key provided by Authorize.net when you signed up for an account.', 'wc-visanet' ),
            ),
            'environment' => array(
                'title'		=> __( 'Authorize.net Test Mode', 'wc-visanet' ),
                'label'		=> __( 'Enable Test Mode', 'wc-visanet' ),
                'type'		=> 'checkbox',
                'description' => __( 'Place the payment gateway in test mode.', 'wc-visanet' ),
                'default'	=> 'no',
            )*/
        );
    }

    // Submit payment and handle response
    public function process_payment( $order_id ) {
        global $woocommerce;

        // Get this Order's information so that we know
        // who to charge and how much
        $customer_order = new WC_Order( $order_id );

        // Get the values we need
        $r['response_code']             = 1;

        // Test the code to know if the transaction went through or not.
        // 1 or 4 means the transaction was a success
        if ( ( $r['response_code'] == 1 ) || ( $r['response_code'] == 4 ) ) {
            // Payment has been successful
            $customer_order->add_order_note( __( 'Visanet payment completed.', 'wc-visanet' ) );

            // Mark order as Paid
            $customer_order->payment_complete();
            update_post_meta( $customer_order->id, 'transaction_id', wc_clean( uniqid() ) );

            // Empty the cart (Very important step)
            $woocommerce->cart->empty_cart();

            // Redirect to thank you page
            return array(
                'result'   => 'success',
                'redirect' => $this->get_return_url( $customer_order ),
            );
        } else {
            // Transaction was not succesful
            // Add notice to the cart
            wc_add_notice( $r['response_reason_text'], 'error' );
            // Add note to the order for your reference
            $customer_order->add_order_note( 'Error: '. $r['response_reason_text'] );
        }

    }

    // Validate fields
    public function validate_fields() {
        return true;
    }

    // Check if we are forcing SSL on checkout pages
    // Custom function not required by the Gateway
    public function do_ssl_check() {
        if( $this->enabled == "yes" ) {
            if( get_option( 'woocommerce_force_ssl_checkout' ) == "no" ) {
                echo "<div class=\"error\"><p>". sprintf( __( "<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>" ), $this->method_title, admin_url( 'admin.php?page=wc-settings&tab=checkout' ) ) ."</p></div>";
            }
        }
    }

}