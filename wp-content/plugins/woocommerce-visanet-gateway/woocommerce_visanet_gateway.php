<?php
/*
Plugin Name: Visanet - WooCommerce Gateway
Plugin URI: http://www.compro.com/
Description: Extends WooCommerce by Adding the Visanet Gateway.
Version: 1.0
Author: Marven
Author URI: http://www.compro.com/
*/

add_action( 'plugins_loaded', 'wc_visanet_init', 0 );
function wc_visanet_init() {
    // If the parent WC_Payment_Gateway class doesn't exist
    // it means WooCommerce is not installed on the site
    // so do nothing
    if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;

    // If we made it this far, then include our Gateway Class
    include_once( 'woocommerce_visanet.php' );

    // Now that we have successfully included our class,
    // Lets add it too WooCommerce
    add_filter( 'woocommerce_payment_gateways', 'wc_visanet_gateway' );
    function wc_visanet_gateway( $methods ) {
        $methods[] = 'WC_Visanet';
        return $methods;
    }
}

// Add custom action links
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_visanet_action_links' );
function wc_visanet_action_links( $links ) {
    $plugin_links = array(
        '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">' . __( 'Settings', 'wc-visanet' ) . '</a>',
    );

    // Merge our new link with the default ones
    return array_merge( $plugin_links, $links );
}