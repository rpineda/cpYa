<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->
</div>
	<?php do_action( 'storefront_before_footer' ); ?>

<?php if (basename(get_the_title()) =="Checkout" || basename(get_the_ID()) == 7 ) { ?>
	</div>
	<div class="row">
		<div class="checkoutfooter">
			<div class="privacylinks">
				<a href="/?page_id=235" target="_blank">Políticas de Privacidad</a> |
				<a href="/?page_id=243" target="_blank"> Políticas de Devolución</a>
			</div>
			<div class="checkoutcopyright">
				©2016. COMPRO-YA. Todos los Derechos Reservados.
			</div>
		</div>
	</div>
<?php } else { ?>
<?php if( basename(get_the_ID()) == 212 || basename(get_the_ID()) ==  "224" || basename(get_the_title()) ==  "Cart"
		|| basename(get_the_title()) ==  "My Account"){?>
<?php } else { ?>
	<div class="container-fluid">
		<div class="wrapnewsletter">
			<!--div class="newslettertitle">Registrate Ya!</div>
			<form class="navbar-form navbar-left newsletterwrapform" role="search">
				<div class="form-group formnewslettergroup">
					<input type="text" class="form-control newsletterform" placeholder="Tu Email">
					<button type="submit" class="btn btn-default newsletterbtnform">Suscribirse</button>
				</div>
			</form>
			<div class="newslettertxt">
				Puedes cancelar tu suscripción cuando quieras
			</div-->
		</div>
	</div>
<?php } ?>
<div class="container-fluid generalfooter">
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="footerlogo visible-xs">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-footer.png" alt="Compro Ya!">
			</div>
			<div class="linksfootercol">
				<a style="font-size: 17px" href="/?page_id=8">Ingresar / Regístrate</a>
			</div>
			<div class="linksfootercol">
				<a href="/">Inicio</a>
				<a href="/?post_type=product">Comprar</a>
				<a href="/?page_id=212">Sobre Nosotros</a>
				<a href="/?page_id=224">Contáctanos</a>
			</div>
			<div class="sociallinks">
				<div class="sociallinks-title">
					Socializa con Nosotros
				</div>
				<a class="social-icons" href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/images/fb-icon-footer.png" alt="Facebook"></a>
				<a class="social-icons" href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/images/tw-icon-footer.png" alt="Twitter"></a>
				<a class="social-icons" href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/images/instagram-icon-footer.png" alt="Instagram"></a>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="footerlogo hidden-xs">
				<img class="footer-logo" src="<?php echo get_template_directory_uri() ?>/assets/images/logo-footer.png" alt="Compro Ya!">
			</div>
			<div class="copyrights-generalfooter">
				©2016. COMPRO-YA. Reservados Todos los Derechos.
				<a href="/?page_id=235">Políticas de Privacidad</a>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<div class="phone-generalfooter">
				Tel. 2313-7700
			</div>
			<div class="emailinfo-generalfooter">
				sac@compro-ya.com<br>
				<img class="sslfooter" src="<?php echo get_template_directory_uri() ?>/assets/images/ssl-footer.png" alt="Tus datos están protegidos por SSL">
			</div>
		</div>
	</div>
</div>

<?php } ?>



			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			//do_action( 'storefront_footer' ); ?>


	<?php //do_action( 'storefront_after_footer' ); ?>

<!-- #page -->

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/scripts.js"></script>

<script src="<?php echo get_template_directory_uri() ?>/assets/js/slick.js"></script>
<script type="text/javascript">
	$('.relatedproducts').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: false
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					infinite: true
				}
			}
		]
	});
</script>
</body>
</html>
