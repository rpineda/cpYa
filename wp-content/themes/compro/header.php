<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php storefront_html_tag_schema(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<link href="<?php echo get_template_directory_uri() ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() ?>/assets/css/imageeffect.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() ?>/assets/css/imageeffect2.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() ?>/assets/css/slick.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() ?>/assets/css/slick-theme.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() ?>/assets/css/style.css" rel="stylesheet">


</head>

<body <?php body_class(); ?>>
<?php if (basename(get_the_title()) =="Checkout" || basename(get_the_ID()) == 7 ) { ?>
<div class="content">
	<div class="row">
		<div class="checkoutheader">
			<div class="col-lg-4 col-md-4 hidden-sm"></div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 checkoutlogo">
				<img class="footer-logo" src="<?php echo get_template_directory_uri() ?>/assets/images/comproya-logo3.png" alt="Compro Ya!">
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 wrapcheckoutheaderinfo">
				<div class="checkoutheaderphone">
					Llámanos: +502 2313-7700
				</div>
				<div class="headerssl-cc">
					<img class="float-right" src="<?php echo get_template_directory_uri() ?>/assets/images/headerssl-cc.jpg" alt="Tus Datos están protegidos por SSL">
				</div>
			</div>
		</div>
			<div class="col-md-12 wrapform">
				<div class="col-lg-3 col-md-1 hidden-sm hidden-xs"></div>
<?php } else { ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<nav class="navbar navbar-default comproheader" role="navigation">
					<div class="header-container">
					<div class="navbar-header">

						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/">
							<img src="<?php echo get_template_directory_uri() ?>/assets/images/comproya-logo3.png" alt="Compro Ya!">
						</a>
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">




						<ul class="nav navbar-nav">
							<li class="active hidden-md hidden-sm">
								<a href="/?post_type=product">Comprar</a>
							</li>
							<li class="hidden-sm">
								<a href="/?page_id=212">Sobre Nosotros</a>
							</li>
							<li class="dropdown subctegory">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Categorías<strong class="caret"></strong></a>
								<ul class="dropdown-menu subctegory-menu">
									<div class="wrapsubmenu">
										<?php //wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>

										<?php //wp_list_pages('title_li=' ); ?>



										<?php  wp_nav_menu_no_ul(); ?>


									</div>
								</ul>
							</li>
						</ul>
						<form class="navbar-form navbar-left wrapsearchbar" method="get" role="search">
							<div class="form-group wrapsearchform">
								<input type="text" class="form-control searchbar" name="s" value="<?php echo $_GET["s"]?> ">
								<input type="hidden" name="post_type" value="product">
								<button type="submit" class="btn btn-default searchbtn">
									<img class="img-buscar" src="<?php echo get_template_directory_uri() ?>/assets/images/icon-search.png" alt="Buscar">
								</button>
							</div>
						</form>
						<ul class="nav navbar-nav navbar-left">
							<li>
								<a href="<?php global $woocommerce;
								$cart_url = $woocommerce->cart->get_cart_url();
								echo $cart_url?>">
									<img class="cart-header" src="<?php echo get_template_directory_uri() ?>/assets/images/icon-cart.png" width="27" height="21" alt="Carrito" valign="middle">
									<?php echo WC()->cart->get_cart_contents_count() < 1 ? "Carrito" : sprintf("Carrito (%s)", WC()->cart->get_cart_contents_count()) ?>
								</a>
							</li>
							<li>
								<?php global $current_user;
								wp_get_current_user(); ?>
								<a href="<?php
								$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
								if ( $myaccount_page_id ) {
									$myaccount_page_url = get_permalink( $myaccount_page_id );
									echo $myaccount_page_url;
								}
								?>"><?php echo is_user_logged_in() ? sprintf('%s %s', $current_user->first_name, $current_user->last_name) : "Ingresar / Registrarse" ?></a>

							</li>
						</ul>
					</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
<?php } ?>
<?php if (basename(get_the_title()) =="home" || basename(get_the_ID()) == 13 ) { ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="jumbotron comprobanner">
				<h2 class="title">
					COMPRO YA!

				</h2>
				<p class="subtitle">
					Donde encuentras de todo
				</p>
				<p>
					<a class="btn btn-primary btn-large btnbanner" href="/?post_type=product">Empieza a Comprar</a>
				</p>
			</div>
		</div>
	</div>
</div>
<?php } elseif (basename(get_the_title()) =="Checkout" || basename(get_the_ID()) == 7 || basename(get_the_ID()) == 224 || basename(get_the_ID()) == 212) { ?>
<?php } else { ?>
				<div class="sectionbanner sectionbanner-product"></div>
</div>
<?php } ?>



<div id="page" class="container">


	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 */
	//do_action( 'storefront_before_content' ); ?>

	<div id="content" class="row" tabindex="-1">
		<div class="col-full">

		<?php
		/**
		 * Functions hooked in to storefront_content_top
		 *
		 * @hooked woocommerce_breadcrumb - 10
		 */
		//do_action( 'storefront_content_top' );
