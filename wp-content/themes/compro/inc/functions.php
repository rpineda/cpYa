<?php
require_once(ABSPATH . 'wp-content/plugins/woocommerce-credom-gateway/vendor/nategood/httpful/bootstrap.php');
/*
	Sync products
 */
function sync_product(){
    require_once(ABSPATH . 'wp-admin/includes/image.php');
    $upload_dir = wp_upload_dir();
    wp_mkdir_p($upload_dir['path']);

	$str = file_get_contents('http://admin.compro-ya.com/products');
	$data = json_decode($str, true);
	foreach ($data['products'] as $product) {
        $post = array(
            'post_author' => '5',
            'post_content' => $product['descripcion'],
            'post_status' => "publish",
            'post_title' => $product['nombre'],
            'post_parent' => '',
            'post_type' => "product",
        );

        //Create post
        $post_id = wp_insert_post( $post );
        

        $image_data = file_get_contents(str_replace(" ","%20", $product['imagen']));
        $filename = basename($product['imagen']);
        $file = $upload_dir['path'] . '/' . $filename;
        file_put_contents($file, $image_data);
        $wp_filetype = wp_check_filetype($filename, null );
        $pos = strpos($file, "/");
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => sanitize_file_name($filename),
            'post_content' => '',
            'post_status' => 'inherit',
            'guid' => get_site_url().'/'.substr($file, $pos+1)
        );

        $attach_id = wp_insert_attachment( $attachment, $file, $post_id );
        add_post_meta($post_id, '_thumbnail_id', $attach_id);

        $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
        $res1= wp_update_attachment_metadata( $attach_id, $attach_data );
        add_post_meta($post_id, '_thumbnail_id', $attach_id);

        $term = get_term_by('name', $product['subcategoria'], 'product_cat');
        wp_set_object_terms( $post_id, $term->term_id, 'product_cat' );
        wp_set_object_terms( $post_id, 'simple', 'product_type');

        $visible = $product['estado'] == 'A' ? 'visible':'insible';

        update_post_meta( $post_id, '_visibility', 'visible' );
        update_post_meta( $post_id, '_stock_status', 'instock');
        update_post_meta( $post_id, 'total_sales', '0');
        update_post_meta( $post_id, '_downloadable', 'no');
        update_post_meta( $post_id, '_virtual', 'no');
        update_post_meta( $post_id, '_regular_price', $product['precio'] );
        update_post_meta( $post_id, '_sale_price', "" );
        update_post_meta( $post_id, '_purchase_note', "" );
        update_post_meta( $post_id, '_featured', "no" );
        update_post_meta( $post_id, '_weight', "" );
        update_post_meta( $post_id, '_length', "" );
        update_post_meta( $post_id, '_width', "" );
        update_post_meta( $post_id, '_height', "" );
        update_post_meta( $post_id, '_sku', $product['sku']);

        $attributes = array();
        array_push($attributes,array (
                'name' => htmlspecialchars( stripslashes( 'unit' ) ), // set attribute name
                'value' => $product['unidad'], // set attribute value
                'position' => 1,
                'is_visible' => 1,
                'is_variation' => 1,
                'is_taxonomy' => 0
            ));
        array_push($attributes,array (
                'name' => htmlspecialchars( stripslashes( 'brand' ) ), // set attribute name
                'value' => $product['marca'], // set attribute value
                'position' => 1,
                'is_visible' => 1,
                'is_variation' => 1,
                'is_taxonomy' => 0
            ));
        update_post_meta( $post_id, '_product_attributes',$attributes);

        update_post_meta( $post_id, '_sale_price_dates_from', "" );
        update_post_meta( $post_id, '_sale_price_dates_to', "" );
        update_post_meta( $post_id, '_price', $product['precio'] );
        update_post_meta( $post_id, '_sold_individually', "" );
        update_post_meta( $post_id, '_manage_stock', "no" );
        update_post_meta( $post_id, '_backorders', "no" );
        update_post_meta( $post_id, '_stock', "" );
    }
}

/*
	Insert order in admin
 */
function sync_insert_order($order){

    $user = array(
        'name' => $order->get_formatted_billing_full_name( ),
        'email'=> $order->billing_email,
        'billingAddress' => $order->get_billing_address(),
        'phone' => $order->billing_phone,
        'nit' => get_user_meta($order->get_user_id(), 'nit', true),
        'reason' => '',
        'city'=> $order->billing_city
    );

    $request = array(
        'orderId'    => $order->id,
        'total'      => $order->order_total,
        'discount' => $order->order_discount,
        'shippingAddress'   => $order->get_shipping_address(),
        'user'   => $user
    );
/**/
    $response = \Httpful\Request::post('http://admin.compro-ya.com/ordered/')
                            ->sendsJson()
                            ->body(json_encode($request))
                            ->send();
    //var_dump($response->body);
    if ($response->body->error) {
        __log('order', 'sync_insert_order_master', $order->id, serialize($request), utf8_encode($response->body->error->message), $response->body->error->code);
        return 0;
    }
    $items = get_items_rest($order);
    $response = \Httpful\Request::post(sprintf('http://admin.compro-ya.com/ordered/%s/items', $order->id))
                    ->sendsJson()
                    ->body(json_encode(array('items' => $items)))
                    ->send();
    //var_dump($response->body);
    if ($response->body->error) {
        __log('order', 'sync_insert_order_detail', $order->id, serialize($request), utf8_encode($response->body->error->message), $response->body->error->code);
        return 0;
    }
}

/*
	Insert detail admin
 */
function get_items_rest($order){
    $cart = $order->get_items();

    $request = array();
    foreach ($cart as $item) {
        $product = new WC_product($item['product_id']);
        $request[] = array(
            'name' => $item['name'],
            'required_quantity' => $item['qty'],
            'price' => $item['line_total']/$item['qty'],
            'total' => $item['line_total'],
            'code' => $product->get_sku()
        );
    }
    return $request;
}
?>