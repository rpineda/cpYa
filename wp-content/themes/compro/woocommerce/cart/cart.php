<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see     http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

<?php do_action( 'woocommerce_before_cart_table' ); ?>

<div class="row">
<div class="col-md-12 breadcrumbs">
	<a href="#">Inicio</a> &gt; Todas las Categorías
</div>
<div class="col-md-9 col-sm-8 col-xs-12 cartleft">
<h1 class="carttitle">
	Tu Carrito de Compras
</h1>
<div class="wrapcarttable">
	<div class="col-md-12 carttitles">
		<div class="col-md-6 col-sm-4 productcarttitle">
			Producto
		</div>
		<div class="col-md-2 col-sm-3 hidden-xs pricecarttitle">
			Precio
		</div>
		<div class="col-md-1 col-sm-1 hidden-xs qtycarttitle">
			Cant.
		</div>
		<div class="col-md-2 col-sm-3 hidden-xs subtotalcarttitle">
			Subtotal
		</div>
		<div class="col-md-1 col-sm-1 hidden-xs removetitle">&nbsp;</div>
	</div>


	<?php
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			?>
	<div class="col-md-12 wrapproductcart">
		<div class="col-md-6 col-sm-4 cartproductinfo">



					<div class="cartimg">
						<?php
						$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

						if ( ! $_product->is_visible() ) {
							echo $thumbnail;
						} else {
							printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
						}
						?>
					</div>
			<div class="carttxt">
				<?php
				if ( ! $_product->is_visible() ) {
					echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
				} else {
					echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
				}

				// Meta data
				echo WC()->cart->get_item_data( $cart_item );

				// Backorder notification
				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
					echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
				}
				?>
				<!--Cart Mobile Info-->
				<div class="visible-xs cartmobileinfo">
					<div class="qty-xs">
						<?php
						if ( $_product->is_sold_individually() ) {
							$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
						} else {
							$product_quantity = woocommerce_quantity_input( array(
								'input_name'  => "cart[{$cart_item_key}][qty]",
								'input_value' => $cart_item['quantity'],
								'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
								'min_value'   => '0'
							), $_product, false );
						}

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
						?>
					</div>
					<div class="wrapxs-pricecart">
						<div class="cartprice-txt-xs">Precio:</div>
						<div class="cartprice-xs">
							<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							?>
						</div>
					</div>
					<div class="wrapxs-subtotalcart">
						<div class="cartsubtotal-txt-xs">Subtotal:</div>
						<div class="cartsubtotal-xs"> <?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?></div>
					</div>
					<div class="wrapremove-xs">Borrar
						<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="newremove" title="%s" data-product_id="%s" data-product_sku="%s">
								<img src="'.get_template_directory_uri() .'/assets/images/icon-removecart.jpg" alt="Borrar">
							</a>',
							esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
							__( 'Remove this item', 'woocommerce' ),
							esc_attr( $product_id ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-2 col-sm-3 cartprice hidden-xs">
			<?php
			echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
			?>
		</div>
		<div class="col-md-1 col-sm-1 cartqty hidden-xs">
			<?php
			if ( $_product->is_sold_individually() ) {
				$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
			} else {
				$product_quantity = woocommerce_quantity_input( array(
					'input_name'  => "cart[{$cart_item_key}][qty]",
					'input_value' => $cart_item['quantity'],
					'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
					'min_value'   => '0'
				), $_product, false );
			}

			echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
			?>
		</div>
		<div class="col-md-2 col-sm-3 cartsubtotal hidden-xs">
			<?php
			echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
			?>
		</div>
		<div class="col-md-1 col-sm-1 remove hidden-xs">
			<?php
			echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
				'<a href="%s" class="newremove" title="%s" data-product_id="%s" data-product_sku="%s">
					<img src="'.get_template_directory_uri() .'/assets/images/icon-removecart.jpg" alt="Borrar">
				</a>',
				esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
				__( 'Remove this item', 'woocommerce' ),
				esc_attr( $product_id ),
				esc_attr( $_product->get_sku() )
			), $cart_item_key );
			?>
		</div>
	</div>


				<?php
				}
			}

			do_action( 'woocommerce_cart_contents' );
			?>




	<div class="col-md-12 carttablefooter">
		<div class="col-md-6 col-sm-5 col-xs-6 continueshopping">
			<a class="return" href="/shop">&lt; Continuar Comprando</a>
			<input type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Actualizar carrito', 'woocommerce' ); ?>" />

			<?php do_action( 'woocommerce_cart_actions' ); ?>

			<?php wp_nonce_field( 'woocommerce-cart' ); ?>

		</div>
		<div class="col-md-3 col-sm-3 hidden-xs subtotaltxt">
			SUBTOTAL
		</div>
		<div class="col-md-2 col-sm-3 col-xs-6 subtotalprice">
			<span class="visible-xs subtotal-xs">SUBTOTAL</span>
			<?php wc_cart_totals_subtotal_html(); ?>
		</div>
		<div class="col-md-1 col-sm-1 hidden-xs removetitle">&nbsp;</div>
	</div>
</div>



<div class="col-md-3 col-sm-4 col-xs-12 cartright visible-xs">
	<div class="wraptotalline">
		<div class="total-subtotaltxt">Subtotal:</div>
		<div class="total-subtotalprice"><?php wc_cart_totals_subtotal_html(); ?></div>
	</div>
	<div class="wraptotalline">
		<div class="total-shippingtxt">Envío:</div>
		<div class="total-shippingprice">
			<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
			<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>
			<?php wc_cart_totals_shipping_html(); ?>
			<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>
			<?php elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>
				<?php _e( 'Shipping', 'woocommerce' ); ?>"><?php woocommerce_shipping_calculator(); ?>
			<?php endif; ?>



		</div>
	</div>
	<div class="wraptotalline">
		<div class="total-totaltxt">TOTAL:</div>
		<div class="total-totalprice">
			<?php wc_cart_totals_order_total_html(); ?>
		</div>
	</div>
	<div class="wrapbtnshoppingcart">
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
	</div>
	<div class="ssl-cc">
		<img class="ssl-cc-logo" src="<?php echo get_template_directory_uri() ?>/assets/images/ssl-cc.jpg" alt="Tus datos están protegidos por SSL">
	</div>
</div>
</div>


<div class="col-md-3 col-sm-4 col-xs-12 cartright hidden-xs">
	<div class="wraptotalline">
		<div class="total-subtotaltxt">Subtotal:</div>
		<div class="total-subtotalprice"><?php wc_cart_totals_subtotal_html(); ?></div>
	</div>
	<div class="wraptotalline">
		<div class="total-shippingtxt">Envío:</div>
		<div class="total-shippingprice">
			<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
				<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>
				<?php wc_cart_totals_shipping_html(); ?>
				<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>
			<?php elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>
				<?php _e( 'Shipping', 'woocommerce' ); ?>"><?php woocommerce_shipping_calculator(); ?>
			<?php endif; ?>

		</div>
	</div>
	<div class="wraptotalline">
		<div class="total-totaltxt">TOTAL:</div>
		<div class="total-totalprice">
			<?php wc_cart_totals_order_total_html(); ?>
		</div>
	</div>
	<div class="wrapbtnshoppingcart">
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
	</div>
	<div class="ssl-cc">
		<img class="ssl-cc-logo" src="<?php echo get_template_directory_uri() ?>/assets/images/ssl-cc-2.jpg" alt="Tus datos están protegidos por SSL">
	</div>
</div>
</div>




<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals">

	<?php do_action( 'woocommerce_cart_collaterals' ); ?>

</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
