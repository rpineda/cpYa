<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

?>
<?php if (is_search() || is_product_category() || is_shop()) {?>
<div class="row">
    <div class="col-md-3 col-sm-4 col-xs-12">
        <div class="navbar navbar-default wrapfilters">
            <div class="filternav">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#filters" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-toggle navbar-brand navbar-filters" href="#" data-toggle="collapse" data-target="#filters">Filtros de Búsqueda</a>
            </div>

            <div class="col-xs-12 collapse navbar-collapse filters" id="filters">
                <?php dynamic_sidebar( 'sidebar-1' ); ?>
            </div>
        </div>

    </div>
    <div class="col-md-9 col-sm-8 col-xs-12">
<?php }?>
<ul class="products">
