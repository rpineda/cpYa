<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

?>

<?php
	// Availability
	$availability      = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>

<?php if ( $product->is_in_stock() ) : ?>



	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<div class="col-xs-12 wrapproductprice">
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12  price">
			<?php echo $product->get_price_html(); ?>
			<meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
			<meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
			<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
			<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 wrapcartbtn">
		<form class="cart" method="post" enctype='multipart/form-data'>
			<div class="qty">
				Cantidad:
				<select name="quantity">
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select>
			</div>
			<div class="wrap-btn-addtocart">
				<button  type="submit" class="btn-addtocart" href="#">
					Agregar a Carrito +
				</button>
			</div>
			<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
		</form>
		</div>
	</div>



	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
