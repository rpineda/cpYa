<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

if ( ! $post->post_excerpt ) {
	//return;
}

?>

<div class="wrapproducttxt">
	<div class="wrapdescriptiontitle">
		<div class="descriptiontitle">
			<div class="descriptiontitle2">
				Descripción
			</div>
		</div>
	</div>

	<p class="descriptionsubtitle">Descripción:</p>
	<p class="descriptiontxt">
		<?php

		global $post;


		?>
	<div itemprop="description">
		<?php the_excerpt(); ?>
	</div>




</div>
<div class="wrapalltags">
</div>
