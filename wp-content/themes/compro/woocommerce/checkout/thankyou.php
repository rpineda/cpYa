<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $order ) : ?>

	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p class="woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

		<p class="woocommerce-thankyou-order-failed-actions">
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
				<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>
		<div class="col-md-2 col-sm-1 hidden-xs"></div>
		<div class="col-md-8 col-sm-10 col-xs-12 wrapthanks">
			<div class="thanksicon">
				<center><img src="<?php echo get_template_directory_uri() ?>/assets/images/icon-comprofacil-envio.png"></center>
			</div>
			<h1 class="thankstitle">
				¡Gracias, tu pedido ha sido recibido!
			</h1>
			<h2 class="thankssubtitle">
				Detalles del pedido
			</h2>

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wrapordernumber">
				<div class="col-md-8 col-sm-8 col-xs-12">
					<div class="ordernumber">
						<span class="greyorder"><?php _e( 'Order Number:', 'woocommerce' ); ?></span> <?php echo $order->get_order_number(); ?>
					</div>
					<div class="orderpaymentinfo">
						<span class="greyorder"><?php _e( 'Payment Method:', 'woocommerce' ); ?></span> <strong> <?php echo $order->payment_method_title; ?></strong>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="orderdate">
						<span class="greyorder"><?php _e( 'Date:', 'woocommerce' ); ?></span> <?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?>
					</div>
					<div class="ordertotal">
						<span class="greyorder"><?php _e( 'Total:', 'woocommerce' ); ?></span> <?php echo $order->get_formatted_order_total(); ?>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="orderdate">
						<span class="greyorder"><?php _e( 'NIT:', 'woocommerce' ); ?></span> <?php echo get_post_meta( $order->id, 'nit', true ); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-2 col-sm-1 hidden-xs"></div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

	<div class="finalordertxt hidden-xs">
		Recibirás un email con la confirmación de tu orden.<br>

		<div class="wrapcontinueshopping-order">
			<a href="/">Continuar Comprando</a>
		</div>
	</div>

<?php else : ?>

	<p class="woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

<?php endif; ?>
