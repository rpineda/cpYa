<?php

/**
 * Storefront automatically loads the core CSS even if using a child theme as it is more efficient
 * than @importing it in the child theme style.css file.
 *
 * Uncomment the line below if you'd like to disable the Storefront Core CSS.
 *
 * If you don't plan to dequeue the Storefront Core CSS you can remove the subsequent line and as well
 * as the sf_child_theme_dequeue_style() function declaration.
 */
//add_action( 'wp_enqueue_scripts', 'sf_child_theme_dequeue_style', 999 );
// Admin Compro Ya
include_once 'inc/functions.php';

/**
 * Dequeue the Storefront Parent theme core CSS
 */
function sf_child_theme_dequeue_style() {
    wp_dequeue_style( 'storefront-style' );
    wp_dequeue_style( 'storefront-woocommerce-style' );
}


add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {
    $currencies['GTQ'] = __( 'Quetzales', 'woocommerce' );
    return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'GTQ': $currency_symbol = 'Q'; break;
    }
    return $currency_symbol;
}

/**
 * Note: DO NOT! alter or remove the code above this text and only add your custom PHP functions below this text.
 */

add_filter( 'woocommerce_default_address_fields', 'billing_postcode' );

function billing_postcode( $address_fields ) {
    $address_fields['postcode']['required'] = false;
    return $address_fields;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
    unset($fields['order']['order_comments']);
    //unset($fields['billing']['billing_company']);
    unset($fields['shipping']['shipping_company']);
    $current_user = wp_get_current_user();

    //var_dump($fields); die;

    $fields['billing']['billing_nit'] = array(
        'label'     => __('NIT', 'woocommerce'),
        'placeholder'   => _x('NIT', 'placeholder', 'woocommerce'),
        'required'  => true,
        'class'     => array('form-row-wide'),
        'clear'     => true,
        'default'   => get_user_meta($current_user->id, 'nit', true)
    );

    $_order = array(
        "billing_nit",
        "billing_first_name",
        "billing_last_name",
        "billing_company",
        "billing_email",
        "billing_phone",
        "billing_country",
        "billing_address_1",
        "billing_address_2",
        "billing_city",
        "billing_state",
        "billing_postcode"

    );

    foreach($_order as $field) {
        $ordered_fields[$field] = $fields["billing"][$field];
    }

    $fields["billing"] = $ordered_fields; //*/

    return $fields;
}

add_action( 'woocommerce_checkout_update_order_meta', 'custom_checkout_field_update_order_meta' );

function custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['billing_nit'] ) ) {
        update_post_meta( $order_id, 'nit', sanitize_text_field( $_POST['billing_nit'] ) );
    }
}

add_action('woocommerce_checkout_update_user_meta', 'custom_checkout_field_update_user_meta');
function custom_checkout_field_update_user_meta( $user_id ) {
    if ($user_id && $_POST['billing_nit']) update_user_meta( $user_id, 'nit', esc_attr($_POST['billing_nit']) );
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'custom_checkout_field_display_admin_order_meta', 10, 1 );

function custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('NIT').':</strong> ' . get_post_meta( $order->id, 'nit', true ) . '</p>';
}

add_filter('woocommerce_email_order_meta_keys', 'custom_order_meta_keys');

function custom_order_meta_keys( $keys ) {
    $keys[] = 'NIT'; // This will look for a custom field called 'Tracking Code' and add it to emails
    return $keys;
}

// display the extra data on order recieved page and my-account order review
function nit_display_order_data( $order_id ){  ?>
    <h2><?php _e( 'Additional Info' ); ?></h2>
    <table class="shop_table shop_table_responsive additional_info">
        <tbody>
        <tr>
            <th><?php _e( 'NIT:' ); ?></th>
            <td><?php echo get_post_meta( $order_id, 'nit', true ); ?></td>
        </tr>
        </tbody>
    </table>
<?php }
#add_action( 'woocommerce_thankyou', 'nit_display_order_data', 20 );
#add_action( 'woocommerce_view_order', 'nit_display_order_data', 20 );

function add_custom_fees($cart) {

    //global $woocommerce;

    /* Grab current total number of products */
    //$number_products = $woocommerce->cart->cart_contents_count;
    $items = $cart->get_cart();

    if (count($items) > 0) {
        $request = array();

        foreach($items as $k => $value) {
            $request[] = array(
                'sku' => $value['data']->get_sku(),
                'price' => get_post_meta($value['product_id'] , '_price', true),
                'qty' => $value['quantity']
            );
        }

        $discount = compro_post_discount($request);

        if ($discount > 0) {
            $cart->add_fee( 'Descuento', -$discount);
        }

        // Alter the cart discount total
        //$cart->discount_cart = $discount;
    }
    //var_dump($cart); die;
}

add_action('woocommerce_cart_calculate_fees' , 'add_custom_fees');


function wp_nav_menu_no_ul()
{
    $options = array(
        'echo' => false,
        'container' => false,
        'theme_location' => 'categories-menu',
        'fallback_cb'=> 'default_page_menu'
    );

    $menu = wp_nav_menu($options);
    echo preg_replace(array(
        '#^<ul[^>]*>#',
        '#</ul>$#'
    ), '', $menu);

}

function categories_menu() {
    register_nav_menu('categories-menu',__( 'Categories Menu' ));
}
add_action( 'init', 'categories_menu' );

function wc_minimum_order_amount() {
    // Set this variable to specify a minimum order value
    $minimum = 250;

    if ( WC()->cart->total < $minimum ) {

        if( is_cart() || is_checkout() ) {

            wc_print_notice(
                sprintf( 'El minimo de tu orden debe de ser de %s para que pueda ser procesada, tu orden actual tiene un total de %s.' ,
                    wc_price( $minimum ),
                    wc_price( WC()->cart->total )
                ), 'error'
            );

        }

        return false;
    }
    return true;
}

add_action('wp_enqueue_scripts', 'override_woo_frontend_scripts');

function override_woo_frontend_scripts() {
    //wp_deregister_script('wc-checkout');
    wp_enqueue_script('wc-checkout', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'), null, true);
}

add_filter( 'mc4wp_form_css_classes', function( $classes ) {
    $classes[] = 'navbar-form navbar-left newsletterwrapform';
    return $classes;
});

/*function add_checkout_script() { ?>

    <script type="text/javascript">

        (function($){
            $(document).on( "updated_checkout", function(){
                $('#place_order').addClass('disabled');
            });
        })(jQuery);

    </script>

<?php
}
add_action( 'woocommerce_after_checkout_form', 'add_checkout_script' );*/