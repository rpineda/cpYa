<?php
/**
 * Storefront template functions.
 *
 * @package storefront
 */

if ( ! function_exists( 'storefront_display_comments' ) ) {
	/**
	 * Storefront display comments
	 *
	 * @since  1.0.0
	 */
	function storefront_display_comments() {
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || '0' != get_comments_number() ) :
			comments_template();
		endif;
	}
}

if ( ! function_exists( 'storefront_html_tag_schema' ) ) {
	/**
	 * Schema type
	 *
	 * @return void
	 */
	function storefront_html_tag_schema() {
		$schema = 'http://schema.org/';
		$type   = 'WebPage';

		if ( is_singular( 'post' ) ) {
			$type = 'Article';
		} elseif ( is_author() ) {
			$type = 'ProfilePage';
		} elseif ( is_search() ) {
			$type 	= 'SearchResultsPage';
		}

		echo 'itemscope="itemscope" itemtype="' . esc_attr( $schema ) . esc_attr( $type ) . '"';
	}
}

if ( ! function_exists( 'storefront_comment' ) ) {
	/**
	 * Storefront comment template
	 *
	 * @param array $comment the comment array.
	 * @param array $args the comment args.
	 * @param int   $depth the comment depth.
	 * @since 1.0.0
	 */
	function storefront_comment( $comment, $args, $depth ) {
		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}
		?>
		<<?php echo esc_attr( $tag ); ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
		<div class="comment-body">
		<div class="comment-meta commentmetadata">
			<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 128 ); ?>
			<?php printf( wp_kses_post( '<cite class="fn">%s</cite>', 'storefront' ), get_comment_author_link() ); ?>
			</div>
			<?php if ( '0' == $comment->comment_approved ) : ?>
				<em class="comment-awaiting-moderation"><?php esc_attr_e( 'Your comment is awaiting moderation.', 'storefront' ); ?></em>
				<br />
			<?php endif; ?>

			<a href="<?php echo esc_url( htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ); ?>" class="comment-date">
				<?php echo '<time>' . get_comment_date() . '</time>'; ?>
			</a>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-content">
		<?php endif; ?>
		<div class="comment-text">
		<?php comment_text(); ?>
		</div>
		<div class="reply">
		<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		<?php edit_comment_link( __( 'Edit', 'storefront' ), '  ', '' ); ?>
		</div>
		</div>
		<?php if ( 'div' != $args['style'] ) : ?>
		</div>
		<?php endif; ?>
	<?php
	}
}

if ( ! function_exists( 'storefront_footer_widgets' ) ) {
	/**
	 * Display the footer widget regions
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function storefront_footer_widgets() {
		if ( is_active_sidebar( 'footer-4' ) ) {
			$widget_columns = apply_filters( 'storefront_footer_widget_regions', 4 );
		} elseif ( is_active_sidebar( 'footer-3' ) ) {
			$widget_columns = apply_filters( 'storefront_footer_widget_regions', 3 );
		} elseif ( is_active_sidebar( 'footer-2' ) ) {
			$widget_columns = apply_filters( 'storefront_footer_widget_regions', 2 );
		} elseif ( is_active_sidebar( 'footer-1' ) ) {
			$widget_columns = apply_filters( 'storefront_footer_widget_regions', 1 );
		} else {
			$widget_columns = apply_filters( 'storefront_footer_widget_regions', 0 );
		}

		if ( $widget_columns > 0 ) : ?>

			<section class="footer-widgets col-<?php echo intval( $widget_columns ); ?> fix">

				<?php
				$i = 0;
				while ( $i < $widget_columns ) : $i++;
					if ( is_active_sidebar( 'footer-' . $i ) ) : ?>

						<section class="block footer-widget-<?php echo intval( $i ); ?>">
							<?php dynamic_sidebar( 'footer-' . intval( $i ) ); ?>
						</section>

					<?php endif;
				endwhile; ?>

			</section><!-- /.footer-widgets  -->

		<?php endif;
	}
}

if ( ! function_exists( 'storefront_credit' ) ) {
	/**
	 * Display the theme credit
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_credit() {
		?>
		<div class="site-info">
			<?php echo esc_html( apply_filters( 'storefront_copyright_text', $content = '&copy; ' . get_bloginfo( 'name' ) . ' ' . date( 'Y' ) ) ); ?>
			<?php if ( apply_filters( 'storefront_credit_link', true ) ) { ?>
			<br /> <?php printf( esc_attr__( '%1$s designed by %2$s.', 'storefront' ), 'Storefront', '<a href="http://www.woothemes.com" alt="Premium WordPress Themes & Plugins by WooThemes" title="Premium WordPress Themes & Plugins by WooThemes" rel="designer">WooThemes</a>' ); ?>
			<?php } ?>
		</div><!-- .site-info -->
		<?php
	}
}

if ( ! function_exists( 'storefront_header_widget_region' ) ) {
	/**
	 * Display header widget region
	 *
	 * @since  1.0.0
	 */
	function storefront_header_widget_region() {
		if ( is_active_sidebar( 'header-1' ) ) {
		?>
		<div class="header-widget-region" role="complementary">
			<div class="col-full">
				<?php dynamic_sidebar( 'header-1' ); ?>
			</div>
		</div>
		<?php
		}
	}
}

if ( ! function_exists( 'storefront_site_branding' ) ) {
	/**
	 * Display Site Branding
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_site_branding() {
		if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) {
			the_custom_logo();
		} elseif ( function_exists( 'jetpack_has_site_logo' ) && jetpack_has_site_logo() ) {
			jetpack_the_site_logo();
		} else { ?>
			<div class="site-branding">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php if ( '' != get_bloginfo( 'description' ) ) { ?>
					<p class="site-description"><?php echo bloginfo( 'description' ); ?></p>
				<?php } ?>
			</div>
		<?php }
	}
}

if ( ! function_exists( 'storefront_primary_navigation' ) ) {
	/**
	 * Display Primary Navigation
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_primary_navigation() {
		?>
		<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_html_e( 'Primary Navigation', 'storefront' ); ?>">
		<button class="menu-toggle" aria-controls="primary-navigation" aria-expanded="false"><span><?php echo esc_attr( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location'	=> 'primary',
					'container_class'	=> 'primary-navigation',
					)
			);

			wp_nav_menu(
				array(
					'theme_location'	=> 'handheld',
					'container_class'	=> 'handheld-navigation',
					)
			);
			?>
		</nav><!-- #site-navigation -->
		<?php
	}
}

if ( ! function_exists( 'storefront_secondary_navigation' ) ) {
	/**
	 * Display Secondary Navigation
	 *
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_secondary_navigation() {
		?>
		<nav class="secondary-navigation" role="navigation" aria-label="<?php esc_html_e( 'Secondary Navigation', 'storefront' ); ?>">
			<?php
				wp_nav_menu(
					array(
						'theme_location'	=> 'secondary',
						'fallback_cb'		=> '',
					)
				);
			?>
		</nav><!-- #site-navigation -->
		<?php
	}
}

if ( ! function_exists( 'storefront_skip_links' ) ) {
	/**
	 * Skip links
	 *
	 * @since  1.4.1
	 * @return void
	 */
	function storefront_skip_links() {
		?>
		<a class="skip-link screen-reader-text" href="#site-navigation"><?php esc_attr_e( 'Skip to navigation', 'storefront' ); ?></a>
		<a class="skip-link screen-reader-text" href="#content"><?php esc_attr_e( 'Skip to content', 'storefront' ); ?></a>
		<?php
	}
}

if ( ! function_exists( 'storefront_page_header' ) ) {
	/**
	 * Display the post header with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_page_header() {
		?>
		<header class="entry-header">
			<?php
			storefront_post_thumbnail( 'full' );
			the_title( '<h1 class="entry-title" itemprop="name">', '</h1>' );
			?>
		</header><!-- .entry-header -->
		<?php
	}
}

if ( ! function_exists( 'storefront_page_content' ) ) {
	/**
	 * Display the post content with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_page_content() {
		?>
		<div class="entry-content" itemprop="mainContentOfPage">
			<?php the_content(); ?>
			<?php
				wp_link_pages( array(
					'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
					'after'  => '</div>',
				) );
			?>
		</div><!-- .entry-content -->
		<?php
	}
}

if ( ! function_exists( 'storefront_post_header' ) ) {
	/**
	 * Display the post header with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_post_header() {
		?>
		<header class="entry-header">
		<?php
		if ( is_single() ) {
			storefront_posted_on();
			the_title( '<h1 class="entry-title" itemprop="name headline">', '</h1>' );
		} else {
			if ( 'post' == get_post_type() ) {
				storefront_posted_on();
			}

			the_title( sprintf( '<h1 class="entry-title" itemprop="name headline"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' );
		}
		?>
		</header><!-- .entry-header -->
		<?php
	}
}

if ( ! function_exists( 'storefront_post_content' ) ) {
	/**
	 * Display the post content with a link to the single post
	 *
	 * @since 1.0.0
	 */
	function storefront_post_content() {
		?>
		<div class="entry-content" itemprop="articleBody">
		<?php
		storefront_post_thumbnail( 'full' );

		the_content(
			sprintf(
				__( 'Continue reading %s', 'storefront' ),
				'<span class="screen-reader-text">' . get_the_title() . '</span>'
			)
		);

		wp_link_pages( array(
			'before' => '<div class="page-links">' . __( 'Pages:', 'storefront' ),
			'after'  => '</div>',
		) );
		?>
		</div><!-- .entry-content -->
		<?php
	}
}

if ( ! function_exists( 'storefront_post_meta' ) ) {
	/**
	 * Display the post meta
	 *
	 * @since 1.0.0
	 */
	function storefront_post_meta() {
		?>
		<aside class="entry-meta">
			<?php if ( 'post' == get_post_type() ) : // Hide category and tag text for pages on Search.

			?>
			<div class="author">
				<?php
					echo get_avatar( get_the_author_meta( 'ID' ), 128 );
					echo '<div class="label">' . esc_attr( __( 'Written by', 'storefront' ) ) . '</div>';
					the_author_posts_link();
				?>
			</div>
			<?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'storefront' ) );

			if ( $categories_list ) : ?>
				<div class="cat-links">
					<?php
					echo '<div class="label">' . esc_attr( __( 'Posted in', 'storefront' ) ) . '</div>';
					echo wp_kses_post( $categories_list );
					?>
				</div>
			<?php endif; // End if categories. ?>

			<?php
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', __( ', ', 'storefront' ) );

			if ( $tags_list ) : ?>
				<div class="tags-links">
					<?php
					echo '<div class="label">' . esc_attr( __( 'Tagged', 'storefront' ) ) . '</div>';
					echo wp_kses_post( $tags_list );
					?>
				</div>
			<?php endif; // End if $tags_list. ?>

		<?php endif; // End if 'post' == get_post_type(). ?>

			<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
				<div class="comments-link">
					<?php echo '<div class="label">' . esc_attr( __( 'Comments', 'storefront' ) ) . '</div>'; ?>
					<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'storefront' ), __( '1 Comment', 'storefront' ), __( '% Comments', 'storefront' ) ); ?></span>
				</div>
			<?php endif; ?>
		</aside>
		<?php
	}
}

if ( ! function_exists( 'storefront_paging_nav' ) ) {
	/**
	 * Display navigation to next/previous set of posts when applicable.
	 */
	function storefront_paging_nav() {
		global $wp_query;

		$args = array(
			'type' 	    => 'list',
			'next_text' => _x( 'Next', 'Next post', 'storefront' ),
			'prev_text' => _x( 'Previous', 'Previous post', 'storefront' ),
			);

		the_posts_pagination( $args );
	}
}

if ( ! function_exists( 'storefront_post_nav' ) ) {
	/**
	 * Display navigation to next/previous post when applicable.
	 */
	function storefront_post_nav() {
		$args = array(
			'next_text' => '%title',
			'prev_text' => '%title',
			);
		the_post_navigation( $args );
	}
}

if ( ! function_exists( 'storefront_posted_on' ) ) {
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function storefront_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s" itemprop="datePublished">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time> <time class="updated" datetime="%3$s" itemprop="datePublished">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			_x( 'Posted on %s', 'post date', 'storefront' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo wp_kses( apply_filters( 'storefront_single_post_posted_on_html', '<span class="posted-on">' . $posted_on . '</span>', $posted_on ), array(
			'span' => array(
				'class'  => array(),
			),
			'a'    => array(
				'href'  => array(),
				'title' => array(),
				'rel'   => array(),
			),
			'time' => array(
				'datetime' => array(),
				'itemprop' => array(),
				'class'    => array(),
			),
		) );
	}
}

if ( ! function_exists( 'storefront_product_categories' ) ) {
	/**
	 * Display Product Categories
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_product_categories( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_product_categories_args', array(
				'limit' 			=> 3,
				'columns' 			=> 3,
				'child_categories' 	=> 0,
				'orderby' 			=> 'name'
				//'title'				=> __( 'Shop by Category', 'storefront' ),
			) );

			echo '<section class="storefront-product-section storefront-product-categories">';

			do_action( 'storefront_homepage_before_product_categories' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'storefront_homepage_after_product_categories_title' );

			echo storefront_do_shortcode( 'product_categories', array(
				'number'  => intval( $args['limit'] ),
				'columns' => intval( $args['columns'] ),
				'orderby' => esc_attr( $args['orderby'] ),
				'parent'  => esc_attr( $args['child_categories'] ),
			) );

			do_action( 'storefront_homepage_after_product_categories' );

			echo '</section>';
		}
	}
}

if ( ! function_exists( 'storefront_wrapquienessomoshome' ) ) {

	function storefront_wrapquienessomoshome( $args ) {
		echo '<div class="wrapquienessomoshome">
                <div class="col-lg-push-6 col-lg-6 col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 col-xs-12 quienessomostxthome">
                    <p align="justify">
                        COMPRO-YA es una Empresa con entregas de productos y proveeduría con pago por medio de Tarjeta de Débito o Crédito. Con compromisos de entrega INMEDIATA.
                    </p>
                    <p align="justify">
                        Tenemos un amplio catálogo donde usted puede encontrar productos para el hogar o la oficina, artículos de limpieza, papelería y útiles, y otras categorías de productos necesarios para la cas​a o​ su negocio.
                    </p>
                    <p>
                        <a class="home-viewmorelink" href="/?page_id=212">Leer más</a>
                    </p>
                </div>
                <div class="col-lg-pull-6 col-lg-6 col-md-6 col-md-pull-6 col-sm-6 col-sm-pull-6 col-xs-12 quienessomosimghome">
                    <img src="'.get_template_directory_uri().'/assets/images/quieneshomeimg-3.jpg" alt="¿Quiénes Somos?">
                </div>
            </div>';
	}

}


if ( ! function_exists( 'storefront_wrapcomprofacil' ) ) {

	function storefront_wrapcomprofacil( $args ) {
		echo '</div><!-- .col-full -->
	</div><!-- #content -->

</div>
</div>
<div class="container-fluid wrapcomprofacil">
        <div class="row">
            <div class="container">
                <div class="comprofaciltitle">
                    Compro Fácil
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 rightborder">
                    <div class="comprofacilblock-img">
                        <img class="comprofacil-img" src="'.get_template_directory_uri().'/assets/images/icon-comprofacil-registrate.png" alt="Regístrate">
                    </div>
                    <div class="comprofacilblock-txt">
                        Regístrate
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 rightborder-compro">
                    <div class="comprofacilblock-img">
                        <img class="comprofacil-img" src="'.get_template_directory_uri().'/assets/images/icon-comprofacil-comprar.png" alt="Compra lo que necesites">
                    </div>
                    <div class="comprofacilblock-txt">
                        Compra lo que necesites
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 rightborder">
                    <div class="comprofacilblock-img">
                        <img class="comprofacil-img" src="'.get_template_directory_uri().'/assets/images/icon-comprofacil-envio.png" alt="Te lo enviamos a tu casa">
                    </div>
                    <div class="comprofacilblock-txt">
                        Te lo enviamos a tu casa
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <div class="comprofacilblock-img">
                        <img class="comprofacil-img" src="'.get_template_directory_uri().'/assets/images/icon-comprofacil-feliz.png" alt="Disfruta de tus artículos">
                    </div>
                    <div class="comprofacilblock-txt">
                        Disfruta de tus artículos
                    </div>
                </div>
			</div>
			</div>
           ';
	}

}


if ( ! function_exists( 'storefront_recent_products' ) ) {
	/**
	 * Display Recent Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_recent_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_recent_products_args', array(
				'limit' 			=> 4,
				'columns' 			=> 4,
				'title'				=> __( 'New In', 'storefront' ),
			) );


			echo '<section class="storefront-product-section storefront-recent-products">';

			do_action( 'storefront_homepage_before_recent_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'storefront_homepage_after_recent_products_title' );

			echo storefront_do_shortcode( 'recent_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );

			do_action( 'storefront_homepage_after_recent_products' );

			echo '</section>';

			$topproducts= '<div class="relatedproducts slider topproducts slick-initialized slick-slider"><button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;">Previous</button>
                <div aria-live="polite" class="slick-list draggable"><div class="slick-track" role="listbox" style="opacity: 1; width: 4688px; transform: translate3d(-2344px, 0px, 0px);"><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="-4" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="-3" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="-2" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="-1" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide" data-slick-index="0" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide00" style="width: 293px;">
                    <div class="imgplp-product view">
                        <div class="productplptag">
                            <img src="images/arrow-offer.png">
                        </div>
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide" data-slick-index="1" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide01" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide" data-slick-index="2" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide02" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide" data-slick-index="3" aria-hidden="true" tabindex="-1" role="option" aria-describedby="slick-slide03" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-current slick-active" data-slick-index="4" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide04" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="0">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="0">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="0">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-active" data-slick-index="5" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide05" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="0">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="0">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="0">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-active" data-slick-index="6" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide06" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="0">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="0">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="0">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-active" data-slick-index="7" aria-hidden="false" tabindex="-1" role="option" aria-describedby="slick-slide07" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="0">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="0">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="0">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="8" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <div class="productplptag">
                            <img src="images/arrow-offer.png">
                        </div>
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="9" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="10" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div><div class="col-md-4 col-xs-6 view-first plpproduct slick-slide slick-cloned" data-slick-index="11" aria-hidden="true" tabindex="-1" style="width: 293px;">
                    <div class="imgplp-product view">
                        <img src="images/img245x245.jpg">
                        <div class="mask">
                            <div class="wrapaddtocartmaskprod">
                                <a href="#" class="info addtocartmaskprod" tabindex="-1">Agregar a Carrito +</a>
                            </div>
                            <div class="wrapviewmoremaskprod">
                                <a href="#" class="info viewmoremaskprod" tabindex="-1">Ver más Detalles</a>
                            </div>
                        </div>
                    </div>
                     <div class="txtplp-product">
                        <a href="#" tabindex="-1">
                            Eget porttitor lorem
                        </a>
                    </div>
                    <div class="priceplp-product">
                        Q.20,000.00
                    </div>
                </div></div></div>







            <button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;">Next</button></div>';
		}
	}
}

if ( ! function_exists( 'storefront_featured_products' ) ) {
	/**
	 * Display Featured Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_featured_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_featured_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'orderby' => 'date',
				'order'   => 'desc',
				'title'   => __( 'We Recommend', 'storefront' ),
			) );

			echo '<section class="storefront-product-section storefront-featured-products">';

			do_action( 'storefront_homepage_before_featured_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'storefront_homepage_after_featured_products_title' );

			echo storefront_do_shortcode( 'featured_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
				'orderby'  => esc_attr( $args['orderby'] ),
				'order'    => esc_attr( $args['order'] ),
			) );

			do_action( 'storefront_homepage_after_featured_products' );

			echo '</section>';
		}
	}
}

if ( ! function_exists( 'storefront_popular_products' ) ) {
	/**
	 * Display Popular Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_popular_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_popular_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'   => __( 'Fan Favorites', 'storefront' ),
			) );

			echo '<section class="storefront-product-section storefront-popular-products">';

			do_action( 'storefront_homepage_before_popular_products' );

			//echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'storefront_homepage_after_popular_products_title' );

			echo storefront_do_shortcode( 'top_rated_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );

			do_action( 'storefront_homepage_after_popular_products' );

			echo '</section>';
		}
	}
}

if ( ! function_exists( 'storefront_on_sale_products' ) ) {
	/**
	 * Display On Sale Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @param array $args the product section args.
	 * @since  1.0.0
	 * @return void
	 */
	function storefront_on_sale_products( $args ) {

		if ( is_woocommerce_activated() ) {

			$args = apply_filters( 'storefront_on_sale_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'   => __( 'On Sale', 'storefront' ),
			) );

			echo '<section class="storefront-product-section storefront-on-sale-products">';

			do_action( 'storefront_homepage_before_on_sale_products' );

			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';

			do_action( 'storefront_homepage_after_on_sale_products_title' );

			echo storefront_do_shortcode( 'sale_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );

			do_action( 'storefront_homepage_after_on_sale_products' );

			echo '</section>';
		}
	}
}

if ( ! function_exists( 'storefront_best_selling_products' ) ) {
	/**
	 * Display Best Selling Products
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since 2.0.0
	 * @param array $args the product section args.
	 * @return void
	 */
	function storefront_best_selling_products( $args ) {
		if ( is_woocommerce_activated() ) {
			$args = apply_filters( 'storefront_best_selling_products_args', array(
				'limit'   => 4,
				'columns' => 4,
				'title'	  => esc_attr__( 'Best Sellers', 'storefront' ),
			) );



			echo '<section class="storefront-product-section storefront-best-selling-products">';
			do_action( 'storefront_homepage_before_best_selling_products' );
			echo '<h2 class="section-title">' . wp_kses_post( $args['title'] ) . '</h2>';
			do_action( 'storefront_homepage_after_best_selling_products_title' );
			echo storefront_do_shortcode( 'best_selling_products', array(
				'per_page' => intval( $args['limit'] ),
				'columns'  => intval( $args['columns'] ),
			) );
			do_action( 'storefront_homepage_after_best_selling_products' );
			echo '</section>';
		}
	}
}

if ( ! function_exists( 'storefront_homepage_content' ) ) {
	/**
	 * Display homepage content
	 * Hooked into the `homepage` action in the homepage template
	 *
	 * @since  1.0.0
	 * @return  void
	 */
	function storefront_homepage_content() {
		while ( have_posts() ) {
			the_post();

			get_template_part( 'content', 'page' );

		} // end of the loop.
	}
}

if ( ! function_exists( 'storefront_social_icons' ) ) {
	/**
	 * Display social icons
	 * If the subscribe and connect plugin is active, display the icons.
	 *
	 * @link http://wordpress.org/plugins/subscribe-and-connect/
	 * @since 1.0.0
	 */
	function storefront_social_icons() {
		if ( class_exists( 'Subscribe_And_Connect' ) ) {
			echo '<div class="subscribe-and-connect-connect">';
			subscribe_and_connect_connect();
			echo '</div>';
		}
	}
}

if ( ! function_exists( 'storefront_get_sidebar' ) ) {
	/**
	 * Display storefront sidebar
	 *
	 * @uses get_sidebar()
	 * @since 1.0.0
	 */
	function storefront_get_sidebar() {
		get_sidebar();
	}
}

if ( ! function_exists( 'storefront_post_thumbnail' ) ) {
	/**
	 * Display post thumbnail
	 *
	 * @var $size thumbnail size. thumbnail|medium|large|full|$custom
	 * @uses has_post_thumbnail()
	 * @uses the_post_thumbnail
	 * @param string $size the post thumbnail size.
	 * @since 1.5.0
	 */
	function storefront_post_thumbnail( $size ) {
		if ( has_post_thumbnail() ) {
			the_post_thumbnail( $size, array( 'itemprop' => 'image' ) );
		}
	}
}

/**
 * The primary navigation wrapper
 */
function storefront_primary_navigation_wrapper() {
	echo '<section class="storefront-primary-navigation">';
}

/**
 * The primary navigation wrapper close
 */
function storefront_primary_navigation_wrapper_close() {
	echo '</section>';
}
